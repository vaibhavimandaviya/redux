import { THEME } from "../constans";

export const setTheme = (data) => {
  return {
    type: THEME,
    payload: data,
  };
};
