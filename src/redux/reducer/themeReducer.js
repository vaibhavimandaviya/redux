import { THEME } from "../constans";

const initialState = { isDark: false };

export default function (state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case THEME:
      return {
        ...state,
        isDark: payload,
      };
    default:
      return state;
  }
}
