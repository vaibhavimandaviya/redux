import logo from "./logo.svg";
import "./App.css";
import { useDispatch, useSelector } from "react-redux";
import { setTheme } from "./redux/action/theme";

function App() {
  const dispatch = useDispatch();
  const mode = useSelector((state) => state.themeReducer);

  return (
    <div className="App" style={{ backgroundColor: mode.isDark ? 'black' : 'white' }}>
      <button
        onClick={() => {
          if (mode.isDark) {
            dispatch(setTheme(false));
          } else {
            dispatch(setTheme(true));
          }
        }}
      >
        {mode.isDark == true ? "light mode" : "dark mode"}
      </button>
    </div>
  );
}

export default App;
